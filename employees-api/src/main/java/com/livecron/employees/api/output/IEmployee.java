package com.livecron.employees.api.output;

import java.util.Date;

/**
 * @author Freddy Aguilar R.
 */
public interface IEmployee {

    Long getId();

    String getFirstName();

    String getLastName();

    Boolean getActive();

    Date getCreatedDate();
}
