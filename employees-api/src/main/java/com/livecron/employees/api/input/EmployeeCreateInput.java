package com.livecron.employees.api.input;

import java.util.Date;

/**
 * @author Freddy Aguilar R.
 */
public class EmployeeCreateInput {

    private Long accoutId;

    private String firstName;

    private String lastName;

    private Boolean active;

    private Date createdDate;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getAccoutId() {
        return accoutId;
    }

    public void setAccoutId(Long accoutId) {
        this.accoutId = accoutId;
    }
}
