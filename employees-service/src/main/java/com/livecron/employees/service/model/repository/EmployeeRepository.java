package com.livecron.employees.service.model.repository;

import com.livecron.employees.service.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Freddy Aguilar R.
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
