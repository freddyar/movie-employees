package com.livecron.employees.service.model.service;

import com.livecron.employees.api.input.EmployeeCreateInput;
import com.livecron.employees.service.model.domain.Employee;
import com.livecron.employees.service.model.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Freddy Aguilar R.
 */

@Service
public class EmployeeCreateService {
    private EmployeeCreateInput input;
    private Employee employee;
    @Autowired
    private EmployeeRepository employeeRepository;

    public void execute(){
        Employee instance = composeEmployeeInstance(input);
        employee = employeeRepository.save(instance);
    }

    private Employee composeEmployeeInstance(EmployeeCreateInput input) {
        Employee instance = new Employee();
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setActive(input.getActive());
        instance.setCreatedDate(input.getCreatedDate());
        instance.setAccountId(input.getAccoutId());
        return instance;
    }

    public void setInput(EmployeeCreateInput input) {
        this.input = input;
    }

    public Employee getEmployee() {
        return employee;
    }
}
